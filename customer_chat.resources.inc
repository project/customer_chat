<?php

/**
 * Returns a list of chat sessions relevant to the logged in user.
 *
 * @param $type
 *   The type of sessions to retrieve. Available values:
 *     'active': Chat sessions assigned to the current agent and unassigned
 *               ones. (default)
 *     'all_active': All active (not closed) chat sessions.
 *     'all': All chat sessions.
 *     'closed': Closed chat sessions.
 * @param $page
 *
 * @return
 *   Array of session objects.
 */
function customer_chat_resource_chat_session_index($type = 'active', $page = 0, $limit = 0) {
  global $user;

  if (!in_array($type, array('active', 'all_active', 'all', 'closed'))) {
    return services_error(t('Invalid request.'), 400);
  }

  $query = db_select('customer_chat_session', 's')
    ->fields('s', array('sid'));

  if ($type == 'closed') {
    $query->condition('closed', 0, '<>');
  }
  else if ($type == 'all_active' || $type == 'active') {
    $query->condition('closed', 0);

    if ($type == 'active') {
      // Requesting active session relevant to the current agents.
      $agent_condition = db_or()
        ->condition('agent_uid', $user->uid)
        ->isNull('agent_uid');

      $query->condition($agent_condition);
    }
  }

  // Add paging, if needed.
  if ($limit) {
    // Include the total in the response.
    $total = $query->countQuery()->execute()->fetchField();

    $start = $page * $limit;
    $query->range($start, $limit);
  }

  $sids = $query->orderBy('sid', 'DESC')
    ->execute()
    ->fetchCol();

  $chat_sessions = customer_chat_session_load_multiple($sids);

  if ($limit) {
    $response = array(
      'total' => $total,
      'items' => services_resource_build_index_list($chat_sessions, 'customer_chat_session', 'sid'),
    );
  }
  else {
    $response = services_resource_build_index_list($chat_sessions, 'customer_chat_session', 'sid');
  }

  return $response;
}

/**
 * Returns a chat session object.
 *
 * @param $sid
 *   A chat session id.
 * @return
 *   Chat session object or FALSE if not found.
 */
function customer_chat_resource_chat_session_retrieve($sid) {

  $chat_session = customer_chat_session_load($sid);

  // Retrieve list of online clients. This will be used to check if the customer
  // is online. Anonymous users cannot be identified individually, so if there is
  // anyone in the chat sessions channel other than the agent, we'll assume the
  // customer is online.
  $online_clients = customer_chat_get_online_clients($sid);
  $agent_uids = customer_chat_get_agents();
  $online_clients = array_diff($online_clients, $agent_uids);
  $chat_session->customer_online = !empty($online_clients) ? 1 : 0;

  // Check if agent is online.
  if (!empty($chat_session->agent_uid)) {
    $chat_session->agent_online = customer_chat_agent_is_online($chat_session->agent_uid) ? 1 : 0;
  }
  else {
    $chat_session->agent_online = 0;
  }

  return $chat_session;
}

/**
 * Access callback for the chat session resource.
 *
 * @param $op
 *   One of view, update, create, delete.
 * @param $args
 *   Resource arguments passed through from the original request.
 *
 * @return
 *   TRUE to allow access; FALSE or services_error()'s return value to deny.
 */
function customer_chat_resource_chat_session_access($op, $args = array()) {
  global $user;

  // Load the chat session for those ops that need one.
  if (in_array($op, array('view', 'start_chat', 'close_chat', 'assign_agent', 'register_agent_targeted', 'post_message'))) {
    $sid = $args[0];
    $chat_session = customer_chat_session_load($sid);

    if (!$chat_session) {
      // Incorrect sid.
      return services_error(t('Chat session not found.'), 404);
    }
  }

  if ($op == 'index') {
    // Only agents can see a list of sessions.
    if (!customer_chat_is_agent()) {
      return services_error(t('Access denied.'), 403);
    }
  }
  else if ($op == 'view') {
    // Agents can view all chat session. Customers can views their own only.
    if (!customer_chat_is_agent() && !customer_chat_is_own_session($chat_session)) {
      return services_error(t('Access denied.'), 403);
    }
  }
  else if ($op == 'register_agent' || $op == 'register_agent_targeted') {
    if (!customer_chat_is_agent()) {
      return services_error(t('Access denied.'), 403);
    }
  }
  else if ($op == 'start_chat') {
    // User can start their own chat sessions.
    if (!customer_chat_is_own_session($chat_session)) {
      return services_error(t('Access denied.'), 403);
    }

    // Check if chat session is already closed.
    if ($chat_session->closed) {
      return services_error(t('The chat session has been closed.'), 403);
    }
  }
  else if ($op == 'close_chat') {
    // Check if chat session is already closed.
    if ($chat_session->closed) {
      return services_error(t('The chat session has already been closed.'), 403);
    }

    // User can close their own chat sessions.
    if (!customer_chat_is_agent() && !customer_chat_is_own_session($chat_session)) {
      return services_error(t('Access denied.'), 403);
    }

    // Agents can close unassigned chats, and chats assigned to them.
    if (customer_chat_is_agent() && $chat_session->agent_uid && $chat_session->agent_uid != $user->uid) {
      return services_error(t('Access denied.'), 403);
    }
  }
  else if ($op == 'post_message') {
    // No posting to closed or unassigned chat sessions.
    if ($chat_session->closed || !$chat_session->agent_uid) {
      return services_error(t('The chat session cannot accept messages.'), 403);
    }

    // User can post to their own chat sessions.
    if (!customer_chat_is_agent() && !customer_chat_is_own_session($chat_session)) {
      return services_error(t('Access denied.'), 403);
    }

    // Agents can post to their own sessions.
    if (customer_chat_is_agent() && $chat_session->agent_uid != $user->uid) {
      return services_error(t('Access denied.'), 403);
    }
  }
  else if ($op == 'assign_agent') {
    $agent_uid = $args[1];
    // Only agents can assign to themselves.
    if (!customer_chat_is_agent() || $user->uid != $agent_uid) {
      return services_error(t('Access denied.'), 403);
    }
  }

  return TRUE;
}

/**
 * Access callback for the agent resource.
 *
 * @param $op
 *   One of view, update, create, delete.
 * @param $args
 *   Resource arguments passed through from the original request.
 *
 * @return bool
 */
function customer_chat_resource_agent_access($op, $args = array()) {
  global $user;

  // @TODO
  return TRUE;
}

/**
 * Posts a messages to a chat session.
 *
 * @param $sid
 *   A chat session id.
 * @param $text
 *   The content of the message.
 */
function customer_chat_resource_chat_session_post_message($sid, $text) {
  global $user;

  $chat_session = customer_chat_session_load($sid);
  if (!$chat_session) {
    // Invalid sid.
    return services_error(t('Invalid chat session id.'), 404);
  }

  if ($user->uid) {
    $uid = $user->uid;
    $sender_name = $user->name;

    // If the sender's user id matches the user that initiated the session, then
    // this user is the customer. Otherwise, it's an agent.
    if ($chat_session->customer_uid == $user->uid) {
      $sender_role = CUSTOMER_CHAT_ROLE_CUSTOMER;
    }
    else {
      $sender_role = CUSTOMER_CHAT_ROLE_AGENT;
    }
  }
  else {
    $uid = NULL;
    $sender_name = $chat_session->customer_name;

    // Anonymous user is always a customer.
    $sender_role = CUSTOMER_CHAT_ROLE_CUSTOMER;
  }

  $record = (object) array(
    'sid' => $sid,
    'sender_role' => $sender_role,
    'sender_uid' => $uid,
    'text' => $text,
    'timestamp' => REQUEST_TIME,
  );

  drupal_write_record('customer_chat_message', $record);

  // Add derived data to the response.
  $record->sender_name = $sender_name;

  // @TODO: Send to appropriate channel only.
  $notification = (object) array(
    'channel' => 'customer_chat_' . $sid,
    'callback' => 'customerChatHandleMessage',
    'data' => $record
  );
  nodejs_send_content_channel_message($notification);


  return $record;
}


/**
 * Assigns the chat session to an agent. Only agents can assign chat sessions
 * to themselves.
 *
 * @param $sid
 *   A chat session id.
 * @param $agent_uid
 *   The uid of the agent that the chat session is being assigned to.
 */
function customer_chat_resource_chat_session_assign_agent($sid, $agent_uid) {
  global $user;

  if (!$agent_uid) {
    // @TODO: Do we need to unassign?
    return services_error(t('Agent id missing.'), 400);
  }

  $agent_account = user_load($agent_uid);
  if (!$agent_account) {
    return services_error(t('Incorrect agent id.'), 400);
  }

  $chat_session = customer_chat_session_load($sid);

  $chat_session->agent_uid = $agent_account->uid;
  $chat_session->agent_name = $agent_account->name;
  $chat_session->agent_online = customer_chat_agent_is_online($agent_account->uid);

  customer_chat_session_save($chat_session);

  // Notify agents.
  $notification = (object) array(
    'channel' => 'customer_chat_agent',
    'callback' => 'customerChatAssignAgent',
    'data' => (array)$chat_session,
  );

  nodejs_send_content_channel_message($notification);

  // Notify customer.
  $notification = (object) array(
    'channel' => 'customer_chat_' . $sid,
    'callback' => 'customerChatAssignAgent',
    'data' => (array)$chat_session,
  );

  nodejs_send_content_channel_message($notification);
}

/**
 * Registers an agent for service. Agents should make an untargeted call
 * (meaning not specifying an sid) after they are ready to accept chats. They
 * should make a call with a specific sid to indicate that they want to receive
 * updates for that specific chat session (this will happen when a new chat
 * session is created after the agent is already online.)
 *
 * @param $sid
 *   A chat session id. Optional.
 */
function customer_chat_resource_chat_session_register_agent($sid = FALSE) {
  global $user;
  $join_channels = array();

  if (!$sid) {
    // Agent just connected.
    // Find session that are relevant to this agent.
    $agent_condition = db_or()
      ->condition('agent_uid', $user->uid)
      ->isNull('agent_uid');

    $results = db_select('customer_chat_session', 's')
      ->fields('s', array('sid', 'agent_uid'))
      ->condition($agent_condition)
      ->condition('closed', 0)
      ->execute();

    foreach ($results as $row) {
      $join_channels[] = 'customer_chat_' . $row->sid;
    }

    // Notify customers that the agent has connected.
    $notification = (object) array(
      'channel' => 'customer_chat_customer',
      'callback' => 'customerChatAgentConnected',
      'data' => array(
        'uid' => $user->uid,
        'name' => $user->name,
      ),
    );
    nodejs_send_content_channel_message($notification);

    // Notify other agents.
    $notification = (object) array(
      'channel' => 'customer_chat_agent',
      'callback' => 'customerChatAgentConnected',
      'data' => array(
        'uid' => $user->uid,
        'name' => $user->name,
      ),
    );
    nodejs_send_content_channel_message($notification);
  }
  else {
    // Agent is registering for one specific chat session.
    // This normally happens when the customer initiates the chat so no need
    // to notify.
    $join_channels[] = 'customer_chat_' . $sid;
  }

  // Generate channel tokens for this agent.
  $channel_tokens = array();
  foreach ($join_channels as $channel_name) {
    $node_response = nodejs_send_content_channel_token($channel_name, TRUE);
    if ($node_response) {
      $channel_tokens[$channel_name] = $node_response->token;
    }
  }

  return array(
    'channel_tokens' => $channel_tokens,
  );
}

/**
 * Notifies participants that the client is ready for chatting. Customers'
 * clients make this call to notify agents of a new client.
 *
 * @param $sid
 *   A chat session id.
 */
function customer_chat_resource_chat_session_start_chat($sid) {
  // Inticate if there are no agents online.
  $online_agents = customer_chat_get_online_agents();
  if (empty($online_agents)) {
    return array('status' => 'no_agent');
  }

  $chat_session = customer_chat_session_load($sid);

  $notification = (object) array(
    'channel' => 'customer_chat_agent',
    'callback' => 'customerChatInitiateChat',
    'data' => array(
      'sid' => $sid,
      'agent_uid' => $chat_session->agent_uid,
    ),
  );
  nodejs_send_content_channel_message($notification);

  return array('status' => 'ok');
}

/**
 * Closes the chat session.
 *
 * @param $sid
 *   A chat session id.
 */
function customer_chat_resource_chat_session_close($sid) {
  db_update('customer_chat_session')
    ->fields(array('closed' => REQUEST_TIME))
    ->condition('sid', $sid)
    ->execute();

  // Notify agents.
  $notification = (object) array(
    'channel' => 'customer_chat_agent',
    'callback' => 'customerChatCloseChat',
    'data' => array(
      'sid' => $sid,
    ),
  );
  nodejs_send_content_channel_message($notification);

  // Notify customer.
  $notification = (object) array(
    'channel' => 'customer_chat_' . $sid,
    'callback' => 'customerChatCloseChat',
    'data' => array(
      'sid' => $sid,
    ),
  );
  nodejs_send_content_channel_message($notification);
}

/**
 * Returns a list of agents.
 *
 * @param $offline
 *   Indicates whether offline agents should be included.
 *
 * @return
 *   Array of agent objects.
 */
function customer_chat_resource_agent_index($offline = FALSE) {
  global $user;

  // Find connected agents.
  $online_agents = customer_chat_get_online_agents();

  if ($offline) {
    // Return all agents.
    $agent_uids = customer_chat_get_agents();
    $agents = user_load_multiple($agent_uids);
  }
  else {
    // Return online agents only.
    $agents = user_load_multiple($online_agents);
  }

  // Add status flag.
  foreach ($agents as $key => $agent) {
    $agent->online = in_array($agent->uid, $online_agents) ? 1 : 0;

    // Remove unnecessary data.
    services_remove_user_data($agent);
  }

  // @TODO: Filter out unnecessary attributes from the user object?

  return services_resource_build_index_list($agents, 'customer_chat_agent', 'uid');
}
