Customer chat module lets you chat with your site's visitors. Privileged users
can use the agent dashboard to handle incoming chat requests and chat with
customers. Multiple agents can be online simultaneously to accept chats. The
module provides a block that conditionally shows a link for visitors to start a
chat when agents are online.

Dependencies
------------

  * jQuery Update (https://www.drupal.org/project/jquery_update)
  * Libraries module (https://www.drupal.org/project/libraries)
  * Node.js integration module (https://www.drupal.org/project/nodejs)
  * Services module (https://www.drupal.org/project/services)
  * Backbone.js (http://backbonejs.org/)
  * Underscore.js (http://underscorejs.org/)

Installation
------------

  * Install module dependencies listed above.
  * Download Backbone.js and place it in the libraries directory so that the
    file is at /sites/all/libraries/backbone/backbone.js.
  * Download Underscore.js and place it in the libraries directory so that the
    file is at /sites/all/libraries/underscore/underscore.js.
  * Go to Configuration>jQuery update (/admin/config/development/jquery_update)
    and ensure that the default jQuery version is set to 1.8 or higher.
  * Set up Node.js integration. This will involve installing a Node.js server
    application. Review the configuration to ensure that Node.js is loaded on
    the customer chat pages (/admin/customer-chat and /customer-chat/*). See the
    module's documentation for instructions.
  * Install and enable this module. Visit the status page at Reports>Status
    report (/admin/reports/status) and ensure that there are no errors.
  * Optional: Go to Configuration>Customer chat
    (/admin/config/system/customer-chat) and select whether the visitor chat
    interface should show the full site theme, or a minimal page without blocks.
  * Optional: Go to the block configuration page, and place "Customer chat:
    Start chat block" in the desired region.
  * Optional: Go to People>Permissions (/admin/people/permissions) and configure
    customer chat permissions as needed.

Usage
-----

  * Agents should open the "Agent dashboard" when they are ready to accept
    incoming chats. The dashboard can be access at /admin/customer-chat, or
    using the link on the default toolbar.
  * When a visitor initiates a new chat, a new tab will appear on the agent
    dashboard showing the visitor's name. After selecting the tab, the agent
    will have an option to either respond to the chat or reject it.
  * When an agent responds to a visitor, that visitor's tab will disappear from
    other agents' dashboards.
  * The "Dashboard" tab will show a list of all active sessions, including those
    assigned to other agents. All agents can view any ongoing chat session.
  * Closed chat sessions can be retrieved using the "View past chat sessions"
    link on the "Dashboard" tab.
  * Site visitors will need to go to /customer-chat/start to initiate a chat.
    The "Start chat block", if enabled, will show a link to this page whenever
    there is an agent online. If the block is not enabled, be sure to place a
    link to this page somewhere on your site so that the visitors can find it.

Support
-------

  Please visit the module's official page to report any issues:
  https://www.drupal.org/project/customer_chat
