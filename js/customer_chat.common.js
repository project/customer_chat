(function($) {

  var customerChatCommon = {};

  customerChatCommon.Models = {};
  customerChatCommon.Collections = {};
  customerChatCommon.Views = {};

  /**
   * Base class for models.
   */
  customerChatCommon.Models.BaseModel = Backbone.Model.extend({
    /**
     * Overrides the Backbone sync function to add the CSRF token.
     */
    sync: function(method, model, options) {
      options = options || {};
      options.headers = options.headers || {};

      if (method == 'action' && options.action) {
        // This method is used to execute targeted actions via the rest api.
        // (Not known by Backbone, so need to specify the necessary options.)
        options.type = 'POST';
        options.url = this.url() + '/' + options.action;
        options.contentType = 'application/json';
        options.data = JSON.stringify(this.toJSON());
      }

      options.headers['X-CSRF-TOKEN'] = Drupal.settings.customer_chat.csrf_token;

      Backbone.Events.trigger('loadingStarted');

      var xhr = Backbone.sync.call(this, method, model, options);

      return xhr
        .done(function (data, textStatus, jqXHR) {
          Backbone.Events.trigger('loadingEnded');
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          var info = [];
          info.push('method: ' + method);
          if (options.action) {
            info.push('action: ' + options.action);
          }
          info.push('error: ' + errorThrown);

          console.log('Request failed (' + info.join(', ') + ')');
          Backbone.Events.trigger('loadingEnded');
        });
    },

    /**
     * Returns the value of a timestamp property as a formatted date/time.
     *
     * @param property
     *   The name of the property to return.
     * @param template
     *   Underscore template to use for formatting. Defaults to m/d/y h:m:s
     */
    formatDateTime: function (property, template) {
      if (!template) {
        template = '<%= month %>/<%= day %>/<%= year %> <%= hour %>:<%= minute %>:<%= second %>';
      }

      return Drupal.customerChat.common.formatDateTime(this.get(property), template, 1);
    }
  });
  
  /**
   * Base class for collections.
   */
  customerChatCommon.Collections.BaseCollection = Backbone.Collection.extend({
    /**
     * Overrides the Backbone sync function.
     */
    sync: function(method, collection, options) {
      Backbone.Events.trigger('loadingStarted');

      var xhr = Backbone.sync.call(this, method, collection, options);

      return xhr
        .done(function (data, textStatus, jqXHR) {
          Backbone.Events.trigger('loadingEnded');
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          var info = [];
          info.push('method: ' + method);
          info.push('error: ' + errorThrown);

          console.log('Request failed (' + info.join(', ') + ')');
          Backbone.Events.trigger('loadingEnded');
        });
    }
  });

  /**
   * Data model for the chat session object.
   */
  customerChatCommon.Models.ChatSessionModel = customerChatCommon.Models.BaseModel.extend({
    urlRoot: '/customer-chat/api/session',
    idAttribute: 'sid',

    initialize: function(data) {
      this.set('messages', new customerChatCommon.Collections.ChatMessageCollection());
    },

    /**
     * Processes the variables sent by the server.
     */
    parse: function (resp, options) {
      if (resp && resp.messages) {
        var messageCollection = new customerChatCommon.Collections.ChatMessageCollection();

        // Wrap all messages in the model, and add to a collection.
        _.each(resp.messages, function (message, i) {
          messageCollection.add(new Drupal.customerChat.common.Models.ChatMessageModel(resp.messages[i]));
        });

        resp.messages = messageCollection;
      }
      return resp;
    },

    /**
     * Adds a new message to the session, and saves it.
     *
     * @param chatMessage
     *   A chat message object.
     * @param skipSave
     *   If true, the message won't be saved to the backend.
     *
     * @return
     *   A jQuery XHR object. Undefined if skipSave is true.
     */
    addMessage: function (chatMessage, skipSave) {
      var chatSessionModel = this;
      var sessionMessages = this.get('messages');

      // Ensure message is linked to this chat session.
      chatMessage.set('sid', this.get('sid'));

      if (skipSave) {
        sessionMessages.add(chatMessage);

        // Trigger event so that UI can update.
        // Note: Cannot trigger 'change' here because that raises an error in the Backbone collection.
        chatSessionModel.trigger('change:messages');
        return;
      }

      // Do not specify any attributes for saving so that the full model will be posted to the backend.
      return chatMessage.save({})
        .done(function (data, textStatus, jqXHR) {
          // Note: We need to wait for the save to complete so that we have the id. That is used to prevent duplicates.
          sessionMessages.add(chatMessage);

          chatSessionModel.trigger('change:messages');
        });
    },

    /**
     * Notifies the server that this session is ready for chatting.
     *
     * @return
     *   A jQuery XHR object.
     */
    startChat: function () {
      var options = {
        action: 'start_chat'
      };

      return this.sync('action', this, options);
    },

    /**
     * Notifies the system that the calling agent is ready for chatting.
     * Note: this is an untargeted action so it can be called without specifying an sid.
     *
     * @return
     *   A jQuery XHR object.
     */
    registerAgent: function () {
      var options = {
        action: 'register_agent'
      };

      return this.sync('action', this, options);
    },

    closeChat: function () {
      var options = {
        action: 'close_chat'
      };

      return this.sync('action', this, options);
    },

    assignAgent: function () {
      var options = {
        action: 'assign_agent'
      };

      return this.sync('action', this, options);
    }

  });

  /**
   * Data model for the chat message object.
   */
  customerChatCommon.Models.ChatMessageModel = customerChatCommon.Models.BaseModel.extend({
    idAttribute: 'mid',

    /**
     * Initializer.
     *
     * @param data
     *   Array of values to set on the model. Expected values:
     *     - data.sid: The id of the chat session this message belongs to.
     *     - data.text: The content of the message.
     */
    initialize: function(data) {
      // Messages are saved by the post_message targeted action api call.
      // @TODO: Can we use .sync to figure out the url?
      this.urlRoot = '/customer-chat/api/session/' + data.sid + '/post_message';
      this.text = data.text;
    }
  });

  /**
   * Data model for the agent object.
   */
  customerChatCommon.Models.AgentModel = customerChatCommon.Models.BaseModel.extend({
    urlRoot: '/customer-chat/api/agent',
    idAttribute: 'uid'
  });

  /**
   * Collection for chat sessions.
   */
  customerChatCommon.Collections.ChatSessionCollection = customerChatCommon.Collections.BaseCollection.extend({
    url: '/customer-chat/api/session',
    model: customerChatCommon.Models.ChatSessionModel,
    total: null,
    comparator: function (model) {
      // This function is needed so that sid is compared as int instead of string.
      return parseInt(model.get('sid'));
    },

    /**
     * Processes the response sent by the server.
     */
    parse: function (models, options) {
      if (options.data && options.data.limit) {
        // If limit is set, the response will contain the items in the .item property.
        this.total = models.total;
        return models.items;
      }

      return models;
    }
  });

  /**
   * Collection for agents.
   */
  customerChatCommon.Collections.AgentCollection = customerChatCommon.Collections.BaseCollection.extend({
    url: '/customer-chat/api/agent',
    model: customerChatCommon.Models.AgentModel
  });

  /**
   * Collection for chat messages.
   */
  customerChatCommon.Collections.ChatMessageCollection = customerChatCommon.Collections.BaseCollection.extend({
    model: customerChatCommon.Models.ChatMessageModel
  });

  /**
   * Base view for the apps. Coordinates subviews.
   */
  customerChatCommon.Views.AppBaseView = Backbone.View.extend({
    events: {},
    subviews: [],

    /**
     * Initializer.
     */
    initialize: function(options) {
    },

    /**
     * Adds a subview.
     *
     * @param view
     *   A view object.
     * @param selector
     *   The selector of the element inside the app view where the subview
     *   should be added with it's rendered.
     */
    addSubview: function (view, selector) {
      var subviewData = {
        view: view,
        selector: selector
      };
      this.subviews.push(subviewData);
      this.renderSubview(subviewData);
    },

    /**
     * Removes a subview.
     *
     * @param view
     *   A view object.
     */
    removeSubview: function (view) {
      for (var i = 0; i < this.subviews.length; i++) {
        if (this.subviews[i].view == view) {
          this.subviews[i].view.remove();
          this.subviews.splice(i, 1);
          break;
        }
      }
    },

    /**
     * Renders all subviews and inserts them into the dom to the corresponding
     * element.
     *
     * @param subviewData
     *   An object containing the following:
     *     - subviewData.view: The view object.
     *     - subviewData.selector: Selector of the element where the subview
     *       should be added.
     */
    renderSubview: function (subviewData) {
      subviewData.view.render();
      // Append the content. (If content has previously been rendered, .append
      // will move the previous element instead of duplicating it.)
      this.$el.find(subviewData.selector).append(subviewData.view.el);
    },

    /**
     * Renders all the subviews.
     */
    render: function () {
      for (var i = 0; i < this.subviews.length; i++) {
        this.renderSubview(this.subviews[i]);
      }
    }

  });

  /**
   * Formats dates/times using a template.
   *
   * @param timestamp
   *   Timestamp to display.
   * @param template
   *   Underscore template used to format the date/time.
   */
  customerChatCommon.formatDateTime = function (timestamp, template) {
    if (timestamp == 0) {
      return '';
    }

    var timeTemplate = _.template(template);

    var date = new Date(timestamp * 1000);
    var params = {
      date: date,
      day: date.getDate(),
      month: date.getMonth() + 1,
      year: date.getFullYear(),
      hour: date.getHours(),
      minute: date.getMinutes(),
      second: date.getSeconds()
    };

    params.minute = (params.minute < 10 ? ('0' + params.minute) : params.minute);
    params.second = (params.second < 10 ? ('0' + params.second) : params.second);

    return timeTemplate(params);
  };

  /**
   * Adds the client's socket to the given channels.
   * This is needed for channels that were joined after the initial connection.
   * Otherwise, the user joins the channels when the socket is connected.
   *
   * @param channels
   *   An object containing channels tokens, keyed by channel name.
   */
  customerChatCommon.joinChannels = function (channels) {
    _.each(channels, function (value, key) {
      if (!Drupal.settings.nodejs.contentTokens[key]) {
        Drupal.settings.nodejs.contentTokens[key] = value;
        Drupal.Nodejs.joinTokenChannel(key, value);
      }
    });
  };

  // Expose to the global Drupal object.
  Drupal.customerChat = Drupal.customerChat || {};
  Drupal.customerChat.common = customerChatCommon;

})(jQuery);
