(function($) {

  var agentApp = {};

  agentApp.Models = {};
  agentApp.Collections = {};
  agentApp.Views = {};

  agentApp.routes = {
    '': 'dashboard',
    'sessions(/:page)': 'sessions',
    'chat/:sid': 'chat',
    '*path': 'notFound'
  };

  /**
   * Initiates the app.
   * 
   * @param element jQuery object of the element that should contain the app.
   */
  agentApp.run = function (element) {
    this.uid = Drupal.settings.customer_chat.uid;
    this.agent_name = Drupal.settings.customer_chat.agent_name;

    this.appView = new agentApp.Views.AppView({el: element});
    this.appView.render();

    // Set up routes
    this.router = new Backbone.Router({
      routes: agentApp.routes
    });

    this.listenTo(this.router, 'route', function (name, args) {
      if (name == 'notFound') {
        // If the path is not found, jump to the dashboard.
        this.router.navigate('', {trigger: true, replace: true});
        return;
      }

      // Delegate to views.
      this.trigger('routeChanged', name, args);
    });

    Backbone.history.start();
  };

  /**
   * View for the visitors list.
   */
  agentApp.Views.TabsView = Backbone.View.extend({
    className: 'ccd-tabs-view',
    chatSessionCollection: null,
    activeTab: null,
    events: {
    },

    initialize: function(options) {
      this.template = _.template($('#ccd-tabs-template').html());

      this.chatSessionCollection = new Drupal.customerChat.common.Collections.ChatSessionCollection();

      // @TODO: Reload on socket.io 'connect'.
      this.chatSessionCollection.fetch({remove: false})
        .done(function (data, textStatus, jqXHR) {
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          alert(Drupal.t('There has been an error while trying to load chat sessions. Please try again.'));
        });

      this.listenTo(agentApp, 'agentAssigned', this.handleAgentAssigned);
      this.listenTo(agentApp, 'initiateChat', this.handleInitiateChat);
      this.listenTo(agentApp, 'closeChat', this.handleCloseChat);
      this.listenTo(agentApp, 'chatViewClosed', this.handleChatViewClosed);
      this.listenTo(agentApp, 'messageReceived', this.handleMessageReceived);
      this.listenTo(agentApp, 'customerDisconnected', this.handleCustomerDisconnected);
      this.listenTo(agentApp, 'routeChanged', this.handleRouteChanged);

      // Listen to add/remove events in the collection.
      this.listenTo(this.chatSessionCollection, 'update', this.render);
      // Listen to changes in the models of the collection.
      this.listenTo(this.chatSessionCollection, 'change', this.render);
    },

    /**
     * Called when the routeChanged event is triggered.
     */
    handleRouteChanged: function (name, args) {
      if (name == 'dashboard') {
        // Route: /
        this.activeTab = 'dashboard'
      }
      else if (name == 'chat') {
        // Route: /chat/[sid]
        this.activeTab = 'chat-' + args[0];
      }
      else {
        // Route with no tab.
        this.activeTab = '';
      }

      this.render();
    },

    /**
     * Called when the initiateChat event is triggered.
     */
    handleInitiateChat: function (chatSession) {
      if (chatSession.get('agent_uid') && chatSession.get('agent_uid') != agentApp.uid) {
        // The session is already assigned to another agent.
        return;
      }

      this.chatSessionCollection.add(chatSession, {merge: true});
    },

    /**
     * Called when the closeChat event is triggered.
     */
    handleCloseChat: function (sid) {
      var chatSession = this.chatSessionCollection.get(sid);
      if (chatSession) {
        if (!chatSession.get('agent_uid')) {
          // If unassigned, remove the tab, otherwise the chat is assigned to
          // the current agent and may have been closed by the customer.
          // Leave the tab in that case.

          this.chatSessionCollection.remove(chatSession.get('sid'));
        }

        chatSession.set('closed', (new Date()).getTime());
      }
    },

    /**
     * Called when the chatViewClosed event is triggered.
     */
    handleChatViewClosed: function (chatSession) {
      // User purposefully closed the tab. Remove chat session.
      this.chatSessionCollection.remove(chatSession.get('sid'));
    },

    /**
     * Called when the agentAssigned event is triggered.
     */
    handleAgentAssigned: function (chatSession) {
      if (chatSession.get('agent_uid') && chatSession.get('agent_uid') != agentApp.uid) {
        // If the chat session got assigned to someone else, it should be removed from the sidebar.
        this.chatSessionCollection.remove(chatSession.get('sid'));
      }
      else {
        // Update the model.
        var existingSession = this.chatSessionCollection.get(chatSession.get('sid'));
        if (existingSession) {
          existingSession.set('agent_uid', chatSession.get('agent_uid'));
          existingSession.set('agent_name', chatSession.get('agent_name'));
        }
      }
    },

    /**
     * Called when the customerDisconnected event is triggered.
     */
    handleCustomerDisconnected: function (sid, uid) {
      var chatSession = this.chatSessionCollection.get(sid);
      if (chatSession && chatSession.get('customer_uid') == uid) {
        chatSession.set('customer_online', 0);
      }
    },

    /**
     * Called when the messageReceived event is triggered.
     */
    handleMessageReceived: function (chatMessage) {
      var chatSession = this.chatSessionCollection.get(chatMessage.get('sid'));
      if (chatSession) {
        chatSession.addMessage(chatMessage, true);

        if (chatMessage.get('sender_uid') != chatSession.get('agent_uid')) {
          chatSession.set('has_new', true);
        }
        else {
          chatSession.set('has_new', false);
        }
      }
    },

    render: function () {
      var rendered = this.template({sessions: this.chatSessionCollection.models, activeTab: this.activeTab});
      this.$el.html(rendered);
      return this;
    }

  });

  /**
   * View for the dashboard.
   */
  agentApp.Views.DashboardView = Backbone.View.extend({
    className: 'ccd-dashboard-view',
    template: null,
    agentCollection: null,
    activeSessionsCollection: null,
    events: {
    },

    initialize: function(options) {
      this.template = _.template($('#ccd-dashboard-template').html());


      // Load agents.
      this.agentCollection = new Drupal.customerChat.common.Collections.AgentCollection();
      this.agentCollection.fetch();

      // Load all active chat sessions.
      this.activeSessionsCollection = new Drupal.customerChat.common.Collections.ChatSessionCollection();
      this.activeSessionsCollection.fetch({data: {type: 'all_active'}})
        .done(function (data, textStatus, jqXHR) {
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
        });

      this.listenTo(this.agentCollection, 'update', this.render);
      this.listenTo(this.activeSessionsCollection, 'update', this.render);
      this.listenTo(this.activeSessionsCollection, 'change', this.render);

      this.listenTo(agentApp, 'initiateChat', this.handleInitiateChat);
      this.listenTo(agentApp, 'closeChat', this.handleCloseChat);
      this.listenTo(agentApp, 'agentConnected', this.handleAgentConnected);
      this.listenTo(agentApp, 'agentAssigned', this.handleAgentAssigned);
      this.listenTo(agentApp, 'agentDisconnected', this.handleAgentDisconnected);
    },

    /**
     * Called when the initiateChat event is triggered.
     */
    handleInitiateChat: function (chatSession) {
      this.activeSessionsCollection.add(chatSession, {merge: true});
    },

    /**
     * Called when the closeChat event is triggered.
     */
    handleCloseChat: function (sid) {
      this.activeSessionsCollection.remove(sid);
    },

    /**
     * Called when the agentConnected event is triggered.
     */
    handleAgentConnected: function (agent) {
      this.agentCollection.add(agent);
    },

    /**
     * Called when the agentAssigned event is triggered.
     */
    handleAgentAssigned: function (chatSession) {
      this.activeSessionsCollection.add(chatSession, {merge: true});
    },

    /**
     * Called when the agentDisconnected event is triggered.
     */
    handleAgentDisconnected: function (uid) {
      this.agentCollection.remove(uid);
    },

    render: function () {
      var rendered = this.template({agents: this.agentCollection, sessions: this.activeSessionsCollection});
      this.$el.html(rendered);
      return this;
    }

  });

  /**
   * View for the session list.
   */
  agentApp.Views.SessionListView = Backbone.View.extend({
    className: 'ccd-session-list-view',
    template: null,
    sessionsCollection: null,
    currentPage: null,
    limit: null,
    events: {
    },

    initialize: function(options) {
      this.sessionsCollection = options.sessionsCollection;
      this.currentPage = options.page || 0;
      this.limit = options.limit || 0;
      this.template = _.template($('#ccd-session-list-template').html());

      this.listenTo(this.sessionsCollection, 'update', this.render);
      this.listenTo(this.sessionsCollection, 'change', this.render);
    },

    render: function () {
      // Pager.
      var totalPages;
      if (this.sessionsCollection.total && this.limit) {
        totalPages = Math.ceil(this.sessionsCollection.total / this.limit);
      }

      var rendered = this.template({sessions: this.sessionsCollection, currentPage: this.currentPage, totalPages: totalPages});
      this.$el.html(rendered);
      return this;
    }

  });

  /**
   * View for the loading indicator.
   */
  agentApp.Views.LoadingIndicatorView = Backbone.View.extend({
    className: 'ccd-loading-indicator-view',
    template: null,
    counter: 0,

    initialize: function(options) {
      this.template = _.template($('#ccd-loading-template').html());

      this.listenTo(Backbone.Events, 'loadingStarted', this.loadingStarted);
      this.listenTo(Backbone.Events, 'loadingEnded', this.loadingEnded);
    },

    loadingStarted: function (id) {
      this.counter++;
      this.render();
    },

    loadingEnded: function (id) {
      this.counter--;
      this.render();
    },

    render: function () {
      if (this.counter > 0) {
        var rendered = this.template();
        this.$el.html(rendered);
      }
      else {
        this.$el.html('');
      }
    }

  });

  /**
   * View for the agent info block.
   */
  agentApp.Views.AgentInfoView = Backbone.View.extend({
    className: 'ccd-agent-info-view',
    template: null,
    status: null,

    initialize: function(options) {
      this.template = _.template($('#ccd-agent-info-template').html());

      this.listenTo(agentApp, 'statusChanged', this.handleStatusChanged);
    },

    /**
     * Called when the statusChanged event is triggered.
     */
    handleStatusChanged: function (status) {
      // Follows the .status attribute in the app view.
      this.status = status;
    },

    render: function () {
      var rendered = this.template({status: this.status});
      this.$el.html(rendered);
    }

  });

  /**
   * View for the chat window.
   */
  agentApp.Views.ChatView = Backbone.View.extend({
    className: 'ccd-chat-view',
    model: null, // ChatSessionModel
    template: null,
    events: {
      'keyup .ccd-text-input': 'keyPressed',
      'click .ccd-send-button': 'sendClicked',
      'click .ccd-end-button': 'endClicked',
      'click .ccd-close-button': 'closeClicked',
      'click .ccd-respond-button': 'respondClicked'
    },

    initialize: function(options) {
      this.model = options.model;
      this.template = _.template($('#ccd-chat-template').html());

      this.listenTo(agentApp, 'messageReceived', this.handleMessageReceived);
      this.listenTo(agentApp, 'initiateChat', this.handleInitiateChat);
      this.listenTo(agentApp, 'customerDisconnected', this.handleCustomerDisconnected);
      this.listenTo(agentApp, 'closeChat', this.handleCloseChat);
      this.listenTo(agentApp, 'agentAssigned', this.handleAgentAssigned);

      // 'messages' is a collection, and changes in its models would not trigger event on this model. Need to trigger
      // it specifically when the collection is manipulated.
      this.listenTo(this.model, 'change:messages', this.render);
      this.listenTo(this.model, 'change', this.render);
    },

    /**
     * Called when the messageReceived event is received.
     */
    handleMessageReceived: function (chatMessage) {
      if (this.model.get('sid') == chatMessage.get('sid')) {
        this.model.addMessage(chatMessage, true);
        this.model.set('has_new', true);
      }
    },

    /**
     * Called when the closeChat event is received.
     */
    handleCloseChat: function (sid) {
      if (this.model.get('sid') == sid) {
        this.model.set('closed', (new Date()).getTime());
      }
    },

    /**
     * Called when the initiateChat event is received.
     */
    handleInitiateChat: function (chatSession) {
      if (this.model.get('sid') == chatSession.get('sid')) {
        // Update attributes. (customer_online will likely change)
        this.model.set(chatSession.attributes);
      }
    },

    /**
     * Called when the customerDisconnected event is received.
     */
    handleCustomerDisconnected: function (sid, uid) {
      if (this.model.get('customer_uid') == uid) {
        this.model.set('customer_online', 0);
      }
    },

    /**
     * Called when the agentAssigned event is received.
     */
    handleAgentAssigned: function (chatSession) {
      if (this.model.get('sid') == chatSession.get('sid')) {
        this.model.set('agent_uid', chatSession.get('agent_uid'));
        this.model.set('agent_name', chatSession.get('agent_name'));
      }
    },

    /**
     * Triggered a key is pressed in the chat textfield.
     *
     * @param e
     *   Javascript event.
     */
    keyPressed: function (e) {
      if (e.keyCode == 13) {
        // Enter key pressed.
        this.sendClicked();
      }
    },

    /**
     * Triggered when the 'send' message button is clicked.
     *
     * @param e
     *   Javascript event.
     */
    sendClicked: function (e) {
      var thisView = this;
      var text = this.$el.find('.ccd-text-input').val();

      if (!text) {
        return;
      }

      this.$el.find('.ccd-text-input').val('');

      // Note: the rest of the attributes will be populated by the backend.
      var chatMessage = new Drupal.customerChat.common.Models.ChatMessageModel({
        sid: this.model.get('sid'),
        text: text
      });

      this.model.addMessage(chatMessage)
        .done(function (data, textStatus, jqXHR) {
          thisView.model.set('has_new', false);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          alert(Drupal.t('Unable to send message: ') + errorThrown);
        });
    },

    /**
     * Triggered when the 'end chat' button is clicked.
     *
     * @param e
     *   Javascript event.
     */
    endClicked: function (e) {
      var chatSession = this.model;
      agentApp.trigger('chatViewClosed', chatSession);
      agentApp.router.navigate('', {trigger: true});

      chatSession.closeChat()
        .done(function (data, textStatus, jqXHR) {
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          alert(Drupal.t('An error occurred while trying to close the chat session: ') + errorThrown);
        });
    },

    /**
     * Triggered when the 'close chat' button is clicked.
     *
     * @param e
     *   Javascript event.
     */
    closeClicked: function (e) {
      var chatSession = this.model;
      agentApp.trigger('chatViewClosed', chatSession);
      agentApp.router.navigate('', {trigger: true});
    },

    /**
     * Triggered when the 'respond' button is clicked.
     *
     * @param e
     *   Javascript event.
     */
    respondClicked: function (e) {
      var thisView = this;
      var originalUid = this.model.get('agent_uid');
      var sid = this.model.get('sid');

      // Set new uid for the call.
      this.model.set('agent_uid', agentApp.uid);

      this.model.assignAgent()
        .done(function (data, textStatus, jqXHR) {
          thisView.model.set('has_new', false);
          thisView.model.set('agent_name', agentApp.agent_name);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          // The new uid has probably not been saved in Drupal. Put back the original uid.
          thisView.model.set('agent_uid', originalUid);
          alert(Drupal.t('There has been an error while trying to assign the chat session: ') + errorThrown);
        });
    },

    render: function () {
      var chatMessages = this.model.get('messages');

      var rendered = this.template({session: this.model, messages: chatMessages, agentUid: agentApp.uid});
      this.$el.html(rendered);
      return this;
    }

  });

  /**
   * View for the main app. Coordinates subviews.
   */
  agentApp.Views.AppView = Drupal.customerChat.common.Views.AppBaseView.extend({
    events: {
    },
    status: null,

    initialize: function(options) {
      var agentInfoView = new agentApp.Views.AgentInfoView();
      this.addSubview(agentInfoView, '.ccd-agent-info');

      var loadingIndicatorView = new agentApp.Views.LoadingIndicatorView();
      this.addSubview(loadingIndicatorView, '.ccd-loading');

      var tabsView = new agentApp.Views.TabsView();
      this.addSubview(tabsView, '.ccd-tabs');

      this.setStatus('offline');

      this.listenTo(agentApp, 'connected', this.handleConnected);
      this.listenTo(agentApp, 'disconnected', this.handleDisonnected);
      this.listenTo(agentApp, 'routeChanged', this.handleRouteChanged);
      this.listenTo(agentApp, 'initiateChat', this.handleInitiateChat);
    },

    /**
     * Sets the current status.
     *
     * @param status
     *   New status.
     *   Possible status values:
     *     - 'offline'
     *     - 'online'
     */
    setStatus: function (status) {
      this.status = status;
      agentApp.trigger('statusChanged', status);
      this.render();
    },

    /**
     * Called when the routeChanged event is triggered.
     *
     * Replaces the main view with the one appropriate for the route.
     */
    handleRouteChanged: function (name, args) {
      // Route: /
      if (name == 'dashboard') {
        var newView = new agentApp.Views.DashboardView();

        this.changeMainView(newView);
      }

      // Route: /chat/[sid]
      if (name == 'chat') {
        var sid = args[0];
        var chatSession = new Drupal.customerChat.common.Models.ChatSessionModel({sid: sid});
        var thisView = this;

        chatSession.fetch()
          .done(function () {
            var newView = new agentApp.Views.ChatView({model: chatSession});
            thisView.changeMainView(newView);
          })
          .fail(function () {
            alert(Drupal.t('There has been an error while trying to load the chat session.'));
          });
      }

      // Route: /sessions/[page]
      if (name == 'sessions') {
        var page = args[0] || 0;
        var limit = 50;
        var thisView = this;

        // Load all chat sessions.
        var sessionsCollection = new Drupal.customerChat.common.Collections.ChatSessionCollection();
        sessionsCollection.fetch({sort: false, data: {type: 'all', page: page, limit: limit}})
          .done(function (data, textStatus, jqXHR) {
            var newView = new agentApp.Views.SessionListView({sessionsCollection: sessionsCollection, page: page, limit: limit});
            thisView.changeMainView(newView);
          })
          .fail(function (jqXHR, textStatus, errorThrown) {
            alert(Drupal.t('There has been an error while trying to load past chat sessions. Please try again.'));
          });

      }
    },

    /**
     * Called when the disconnected event is triggered.
     */
    handleDisonnected: function () {
      this.setStatus('offline');
    },

    /**
     * Called when the connected event is triggered.
     *
     * Prepares the session for chatting. Refreshes the chat session list
     * and signals to the server that the agent is ready for chatting.
     */
    handleConnected: function () {
      var thisView = this;

      // Notify other users that the agent has connected.
      // Note: This is needed because the fact that the agent connects to
      // nodejs does not mean they are on the agent dashboard.
      var chatSessionModel = new Drupal.customerChat.common.Models.ChatSessionModel();
      chatSessionModel.registerAgent()
        .done(function (data, textStatus, jqXHR) {
          // Register agent succeeded.
          // The call returns tokens that we can use to attach this agent's socket to the sessions we need to
          // receive messages from.
          Drupal.customerChat.common.joinChannels(data.channel_tokens);
          thisView.setStatus('online');
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          alert(Drupal.t('There has been an error while trying to initiate connection. Please try again.'));
        });
    },

    /**
     * Called when a new chat is initiated.
     *
     * Registers the agent in the chat's channel so that the agent receives
     * events.
     */
    handleInitiateChat: function (chatSession) {
      chatSession.registerAgent()
        .done(function (data, textStatus, jqXHR) {
          // Register agent succeeded.
          // The call returns tokens that we can use to attach this agent's socket to the sessions we need to
          // receive messages from.

          Drupal.customerChat.common.joinChannels(data.channel_tokens);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          alert(Drupal.t('There has been an error while trying to initiate chat.'));
        });
    },

    changeMainView: function (newView) {
      if (!newView) {
        return;
      }

      this.removeSubview(this.mainView);
      this.mainView = newView;
      this.addSubview(this.mainView, '.ccd-main-content');

      agentApp.trigger('mainViewChanged');
    }

  });

  // Add event capability to the app object.
  _.extend(agentApp, Backbone.Events);

  // Expose to the global Drupal object.
  Drupal.customerChat.agentApp = agentApp;

  /**
   * Called when a customer initiates a chat.
   * Adds a new tab for the pending customer.
   */
  Drupal.Nodejs.callbacks.customerChatInitiateChat = {
    callback: function (message) {
      var chatSession = new Drupal.customerChat.common.Models.ChatSessionModel(message.data);


      chatSession.fetch()
        .done(function (data, textStatus, jqXHR) {
          agentApp.trigger('initiateChat', chatSession);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          alert(Drupal.t('An error occurred while trying to load the chat session: ') + errorThrown);
        });
    }
  };

  /**
   * Called when an agent connects.
   */
  Drupal.Nodejs.callbacks.customerChatAgentConnected = {
    callback: function (message) {
      var agent = new Drupal.customerChat.common.Models.AgentModel(message.data);
      agentApp.trigger('agentConnected', agent);
    }
  };

  /**
   * Called when a new message is received.
   */
  Drupal.Nodejs.callbacks.customerChatHandleMessage = {
    callback: function (message) {

      var chatMessage = new Drupal.customerChat.common.Models.ChatMessageModel(message.data);
      agentApp.trigger('messageReceived', chatMessage);
    }
  };

  /**
   * Called when a chat session is assigned to an agent.
   */
  Drupal.Nodejs.callbacks.customerChatAssignAgent = {
    callback: function (message) {
      var chatSession = new Drupal.customerChat.common.Models.ChatSessionModel(message.data);
      agentApp.trigger('agentAssigned', chatSession);
    }
  };

  /**
   * Called when a chat session is closed.
   */
  Drupal.Nodejs.callbacks.customerChatCloseChat = {
    callback: function (message) {
      // Chat could have been closed by the customer or the agent.
      agentApp.trigger('closeChat', message.data.sid);
    }
  };

  /**
   * Called when a client disconnects from the channel that we are in.
   */
  Drupal.Nodejs.contentChannelNotificationCallbacks.customerChatContentChannelHandler = {
    callback: function (message) {
      if (message.data.type == 'disconnect') {
        if (message.channel.substr(0, 14) == 'customer_chat_') {
          var channelId = message.channel.substr(14);

          if (channelId == 'agent') {
            agentApp.trigger('agentDisconnected', message.data.uid);
          }
          else {
            agentApp.trigger('customerDisconnected', channelId, message.data.uid);
        }
      }
    }
    }
  };

  /**
   * Connection handlers for the socket.io connection.
   */
  Drupal.Nodejs.connectionSetupHandlers.customerChatConnectionHandler = {
    /**
     * Called when the socket has connected.
     * Note: authentication may not have happened yet.
     */
    connect: function () {
      agentApp.trigger('connected');
    },

    /**
     * Called when the socket has disconnected.
     */
    disconnect: function () {
      agentApp.trigger('disconnected');
    }
  };

})(jQuery);
