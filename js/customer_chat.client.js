(function($) {

  var clientApp = {};

  clientApp.Models = {};
  clientApp.Collections = {};
  clientApp.Views = {};

  /**
   * Initiates the app.
   *
   * @param element
   *   jQuery object of the element that should contain the app.
   */
  clientApp.run = function (element) {
    this.sid = Drupal.settings.customer_chat.sid;
    this.chatSession = new Drupal.customerChat.common.Models.ChatSessionModel({sid: this.sid});

    this.appView = new clientApp.Views.AppView({el: element});
    this.appView.render();
  };

  /**
   * View for the chat window.
   */
  clientApp.Views.ChatView = Backbone.View.extend({
    className: 'cc-chat-view',
    status: null,
    template: null,
    events: {
      'keyup .cc-text-input': 'keyPressed',
      'click .cc-send-button': 'sendClicked',
      'click .cc-end-button': 'endClicked'
    },

    initialize: function(options) {
      var thisView = this;
      this.template = _.template($('#cc-chat-template').html());

      this.setStatus('connecting');

      clientApp.chatSession.fetch()
        .done(function (data, textStatus, jqXHR) {

          // Successfully fetched the chat session. Trigger start.
          if (clientApp.chatSession.get('closed') == 0) {
            clientApp.chatSession.startChat()
              .done(function (data, textStatus, jqXHR) {
                if (data.status == 'no_agent') {
                  // No agents online.
                  thisView.setStatus('noAgent');
                }
                else {
                  thisView.setStatus('ready');
                }
              })
              .fail(function (jqXHR, textStatus, errorThrown) {
                thisView.setStatus('loadError');
              });
          }
          else {
            thisView.setStatus('ready');
          }

        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          // Fetch call failed due to network error or access denied.
          // (access has already been checked when the page loaded; no need to handle here)
          thisView.setStatus('loadError');
        });

      this.listenTo(clientApp, 'agentConnected', function () {
        if (thisView.status == 'noAgent') {
          thisView.setStatus('ready');
        }
      });

      this.listenTo(clientApp.chatSession, 'change', this.render);
      this.listenTo(clientApp.chatSession, 'change:messages', this.render);
    },

    /**
     * Sets the current status.
     *
     * @param status
     *   New status.
     *   Possible status values:
     *     - 'connecting': Client is connecting to retrieve chat session info.
     *     - 'loadError': Network error, or error received from the server.
     *     - 'noAgent': There are no agents online.
     *     - 'ready': The chat session has been initialized.
     */
    setStatus: function (status) {
      this.status = status;
      this.render();
    },

    /**
     * Triggered when a key is pressed in the input field.
     *
     * @param e
     *   Javascript event object.
     */
    keyPressed: function (e) {
      if (e.keyCode == 13) {
        // Enter key pressed.
        this.sendClicked();
      }
    },

    /**
     * Triggered when the send message button is clicked.
     *
     * @param e
     *   Javascript event object.
     */
    sendClicked: function (e) {
      var text = this.$el.find('.cc-text-input').val();
      if (!text) {
        return;
      }

      this.$el.find('.cc-text-input').val('');

      var chatMessage = new Drupal.customerChat.common.Models.ChatMessageModel({
        sid: clientApp.sid,
        text: text
      });

      clientApp.chatSession.addMessage(chatMessage)
        .fail(function (data, textStatus, jqXHR) {
          // @TODO: error check
          console.log('Unable to save message: ' + textStatus);
        });

    },

    /**
     * Triggered when the end chat button is clicked.
     *
     * @param e
     *   Javascript event object.
     */
    endClicked: function (e) {
      clientApp.chatSession.closeChat();
      clientApp.chatSession.set('closed', (new Date()).getTime());
    },

    /**
     * Renders the view. Formats the message times.
     */
    render: function () {
      var chatMessages = clientApp.chatSession.get('messages');

      var rendered = this.template({chatSession: clientApp.chatSession, messages: chatMessages, status: this.status});
      this.$el.html(rendered);
      return this;
    }

  });

  /**
   * View for the main app.
   */
  clientApp.Views.AppView = Drupal.customerChat.common.Views.AppBaseView.extend({
    initialize: function(options) {
      var chatView = new clientApp.Views.ChatView();
      this.addSubview(chatView, '.cc-chat');
    }
  });

  // Add event functionality to the app object.
  _.extend(clientApp, Backbone.Events);

  // Expose to the global Drupal object.
  Drupal.customerChat.clientApp = clientApp;

  /**
   * Called when a new message is received.
   */
  Drupal.Nodejs.callbacks.customerChatHandleMessage = {
    callback: function (message) {
      var chatMessage = new Drupal.customerChat.common.Models.ChatMessageModel(message.data);
      clientApp.chatSession.addMessage(chatMessage, true);
    }
  };

  /**
   * Called when the chat session is assigned.
   */
  Drupal.Nodejs.callbacks.customerChatAssignAgent = {
    callback: function (message) {
      clientApp.chatSession.set({
        agent_uid: message.data.agent_uid,
        agent_online: message.data.agent_online,
        agent_name: message.data.agent_name
      });
    }
  };

  /**
   * Called a chat session is closed.
   */
  Drupal.Nodejs.callbacks.customerChatCloseChat = {
    callback: function (message) {
      clientApp.chatSession.set({
        closed: (new Date().getTime())
      });
    }
  };

  /**
   * Called when the agent assigned to the active chat session connects.
   */
  Drupal.Nodejs.callbacks.customerChatAgentConnected = {
    callback: function (message) {

      clientApp.trigger('agentConnected');

      if (clientApp.chatSession.get('agent_uid') == message.data.uid) {
        clientApp.chatSession.set({
          agent_online: 1
        });
      }
    }
  };

  /**
   * Called when users in the channel disconnect.
   */
  Drupal.Nodejs.contentChannelNotificationCallbacks.customerChatChannelNotification = {
    callback: function (message) {
      if (message.data.uid == clientApp.chatSession.get('agent_uid')) {
      clientApp.chatSession.set({
        agent_online: 0
      });
    }
    }
  };

})(jQuery);
