(function($) {

  /**
   * Drupal behavior.
   */
  Drupal.behaviors.customerChat = {
    attach: function (context, settings) {
      if(context != document) return;

      // Initialize agent dashboard when on the dashboard page.
      if (Drupal.customerChat.agentApp && $('#customer-chat-dashboard')) {
        Drupal.customerChat.agentApp.run($('#customer-chat-dashboard'));
      }

      // Initialize client when on the client chat page.
      if (Drupal.customerChat.clientApp && $('#customer-chat-client')) {
        Drupal.customerChat.clientApp.run($('#customer-chat-client'));
      }
    }
  };

})(jQuery);
