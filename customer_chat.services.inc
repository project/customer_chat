<?php

/**
 * Implements hook_default_services_endpoint().
 */
function customer_chat_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE;
  $endpoint->api_version = 3;
  $endpoint->name = 'customer_chat';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'customer-chat/api';
  $endpoint->authentication = array(
    'services' => 'services',
  );
  $endpoint->server_settings = array(
    'formatters' => array(
      'json' => TRUE,
      'bencode' => FALSE,
      'jsonp' => FALSE,
      'php' => FALSE,
      'xml' => FALSE,
    ),
    'parsers' => array(
      'application/json' => TRUE,
      'application/x-www-form-urlencoded' => TRUE,
      'application/vnd.php.serialized' => FALSE,
      'application/xml' => FALSE,
      'multipart/form-data' => FALSE,
      'text/xml' => FALSE,
    ),
  );
  $endpoint->resources = array(
    'customer_chat_agent' => array(
      'alias' => 'agent',
      'operations' => array(
        'index' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'customer_chat_session' => array(
      'alias' => 'session',
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
        'create' => array(
          'enabled' => '1',
        ),
        'update' => array(
          'enabled' => '1',
        ),
        'index' => array(
          'enabled' => '1',
        ),
      ),
      'actions' => array(
        'register_agent' => array(
          'enabled' => '1',
        ),
      ),
      'targeted_actions' => array(
        'post_message' => array(
          'enabled' => '1',
        ),
        'register_agent' => array(
          'enabled' => '1',
        ),
        'assign_agent' => array(
          'enabled' => '1',
        ),
        'start_chat' => array(
          'enabled' => '1',
        ),
        'close_chat' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'user' => array(
      'actions' => array(
        'token' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 1;
  $export['customer_chat'] = $endpoint;

  return $export;
}
