<?php

/**
 * @file
 * Default theme implementation for the chat window.
 *
 * Available variables:
 * - $page: The regular page array (containing regions, etc.)
 * - $page['chat_window_content']: The main content of the chat window.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $classes: Classes prepared in $classes_array, turned into a string.
 *   Contains the theme name by default.
 *
 * General utility variables (same as the page template):
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 *
 * @see template_preprocess()
 * @see template_process()
 * @see customer_chat_preprocess_customer_chat_window()
 * @see customer_chat_process_customer_chat_window()
 */
?>

<div class="<?php print $classes; ?>">
  <div id="page">

    <div id="header">
      <div class="section clearfix">

        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Logo'); ?>" />
          </a>
        <?php endif; ?>

        <?php if ($site_name): ?>
          <div id="name-and-slogan">
            <h1 id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
                <?php print $site_name; ?>
              </a>
            </h1>
          </div> <!-- /#name-and-slogan -->
        <?php endif; ?>

      </div> <!-- /.section -->
    </div> <!-- /#header -->

    <?php print $messages; ?>

    <div id="main-wrapper">
      <div id="main" class="clearfix">

        <div id="content" class="column">
          <div class="section">
            <a id="main-content"></a>

            <h1 class="title" id="page-title"><?php print $title; ?></h1>

            <?php print render($page['chat_window_content']); ?>

          </div> <!-- /.section -->
        </div> <!-- /#content -->

      </div> <!-- /#main -->
    </div> <!-- /#main-wrapper -->

  </div> <!-- /#page -->
</div>
