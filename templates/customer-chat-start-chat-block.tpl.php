<div class="customer-chat-start-chat-block">
  <?php if (!$online): ?>
    <span><?php print $offline_message; ?></span>
  <?php else: ?>
    <span><?php print $start_link; ?></span>
  <?php endif; ?>
</div>
