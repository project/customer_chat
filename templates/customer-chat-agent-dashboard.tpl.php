<div id="customer-chat-dashboard">

  <div class="ccd-sidebar">
    <div class="ccd-loading"></div>

    <div class="ccd-section">
      <div class="ccd-agent-info"></div>
    </div>

    <div class="ccd-section">
      <div class="ccd-tabs"></div>
    </div>

  </div>

  <div class="ccd-main-content">
  </div>

</div>


<!-- Template for the agent info view -->
<script type="text/template" id="ccd-agent-info-template">
  <h2><?php print t('Customer chat'); ?></h2>

  <div class="ccd-status">
    <% if (status == 'online') { %>
      <span class="ccd-status-indicator ccd-status-online"><?php print t('Online'); ?></span>
    <% } else { %>
      <span class="ccd-status-indicator ccd-status-offline"><?php print t('Offline'); ?></span>
    <% } %>
  </div>
</script>

<!-- Template for the dashboard -->
<script type="text/template" id="ccd-dashboard-template">
  <h2><?php print t('Dashboard'); ?></h2>

  <h3 class="ccd-table-title"><?php print t('Online agents'); ?></h3>
  <table class="ccd-table">
    <thead>
      <tr>
        <th><?php print t('Agent name'); ?></th>
      </tr>
    </thead>

    <tbody>
      <% agents.each(function(agent, index) { %>
        <tr class="<%= (index % 2 == 0) ? 'even' : 'odd' %>"><td><%= agent.get('name') %></td></tr>
      <% }) %>
    </tbody>
  </table>

  <h3 class="ccd-table-title"><?php print t('Active sessions'); ?></h3>
  <table class="ccd-table">
    <thead>
      <tr>
        <th><?php print t('Customer name'); ?></th>
        <th><?php print t('Customer email'); ?></th>
        <th><?php print t('Assigned agent'); ?></th>
        <th><?php print t('Started at'); ?></th>
        <th><?php print t('Client IP'); ?></th>
        <th><?php print t('Client OS'); ?></th>
        <th><?php print t('Client browser'); ?></th>
      </tr>
    </thead>

    <tbody>
      <% if (sessions.length == 0) { %>
        <tr>
          <td colspan="7"><?php print t('There are no active chat sessions.'); ?></td>
        </tr>
      <% } else { %>
        <% sessions.each(function(session, index) { %>
          <tr class="<%= (index % 2 == 0) ? 'even' : 'odd' %>">
            <td><a href="#chat/<%= session.get('sid') %>"><%- session.get('customer_name') %></a></td>
            <td><%- session.get('customer_email') %></td>
            <td><%- session.get('agent_name') %></td>
            <td><%= session.formatDateTime('created') %></td>
            <td><%- session.get('ip') %></td>
            <td><%- session.get('os') %></td>
            <td><%- session.get('browser') %></td>
          </tr>
        <% }) %>
      <% } %>
    </tbody>
  </table>

  <a href="#/sessions"><?php print t('View past chat sessions'); ?></a>

</script>

<!-- Template for the tabs view -->
<script type="text/template" id="ccd-tabs-template">
  <div class="ccd-tab<%= (activeTab == 'dashboard' ? ' active' : '') %>">
    <a href="#">Dashboard</a>
  </div>

  <% _.each(sessions, function(session) { %>
    <div class="ccd-tab<%= (activeTab == ('chat-' + session.get('sid')) ? ' active' : '') %>">
      <% if (session.get('has_new')) { %>
        <span class="ccd-new-message-icon"></span>
      <% } else { %>
        <span class="ccd-new-message-icon ccd-inactive"></span>
      <% } %>
      <a href="#/chat/<%= session.get('sid') %>"><%- session.get('customer_name') %></a>
    </div>
  <% }) %>
</script>

<!-- Template for the session list window -->
<script type="text/template" id="ccd-session-list-template">
  <h3 class="ccd-table-title"><?php print t('All sessions'); ?></h3>
  <table class="ccd-table">
    <thead>
    <tr>
      <th><?php print t('Customer name'); ?></th>
      <th><?php print t('Customer email'); ?></th>
      <th><?php print t('Assigned agent'); ?></th>
      <th><?php print t('Started at'); ?></th>
      <th><?php print t('Closed at'); ?></th>
      <th><?php print t('Client IP'); ?></th>
      <th><?php print t('Client OS'); ?></th>
      <th><?php print t('Client browser'); ?></th>
    </tr>
    </thead>

    <tbody>
    <% if (sessions.length == 0) { %>
    <tr>
      <td colspan="8"><?php print t('There are no chat sessions.'); ?></td>
    </tr>
    <% } else { %>
    <% sessions.each(function(session, index) { %>
    <tr class="<%= (index % 2 == 0) ? 'even' : 'odd' %>">
      <td><a href="#chat/<%= session.get('sid') %>"><%- session.get('customer_name') %></a></td>
      <td><%- session.get('customer_email') %></td>
      <td><%- session.get('agent_name') %></td>
      <td><%= session.formatDateTime('created') %></td>
      <td><%= session.formatDateTime('closed') %></td>
      <td><%- session.get('ip') %></td>
      <td><%- session.get('os') %></td>
      <td><%- session.get('browser') %></td>
    </tr>
    <% }) %>
    <% } %>
    </tbody>
  </table>

  <div class="item-list">
    <ul class="pager">
      <% for (var page = 0; page < totalPages; page++) { %>
        <% if (currentPage == page) { %>
          <li class="pager-item pager-current"><%= (page + 1) %></li>
        <% } else { %>
          <li class="pager-item"><a href="#/sessions/<%= page %>"><%= (page + 1) %></a></li>
        <% } %>
      <% } %>
    </ul>
  </div>

</script>

<!-- Template for the chat window -->
<script type="text/template" id="ccd-chat-template">

  <% if (!session.get('customer_profile_link')) { %>
    <h2><%- session.get('customer_name') %> (<%- session.get('customer_email') %>)</h2>
  <% } else { %>
    <h2><a href="<%= session.get('customer_profile_link') %>"><%- session.get('customer_name') %> (<%- session.get('customer_email') %>)</a></h2>
  <% } %>

  <div class="ccd-session-info">
    <div class="ccd-session-custom-data">
      <%= session.get('custom_data') %>
    </div>

    <div class="ccd-chat-status">
      <% if (session.get('closed') != 0) { %>
        <?php print t('This chat session has been closed.'); ?>
      <% } else if (session.get('customer_online') == 0) { %>
        <?php print t('The customer is offline.'); ?>
      <% } else { %>
        <?php print t('Connected.'); ?>
      <% } %>
    </div>

    <div class="ccd-session-agent-name">
      <?php print t('Agent'); ?>: <%- session.get('agent_name') %>
    </div>

    <div class="cc-chat-buttons">
      <% if (session.get('closed') == 0 && !session.get('agent_uid')) { %>
        <input type="button" class="ccd-respond-button form-submit" value="<?php print t('Respond'); ?>">
        <input type="button" class="ccd-end-button form-submit" value="<?php print t('Reject'); ?>">
      <% } else if (session.get('closed') == 0 && session.get('agent_uid') == agentUid) { %>
        <input type="button" class="ccd-end-button form-submit" value="<?php print t('End chat'); ?>">
      <% } else { %>
        <input type="button" class="ccd-close-button form-submit" value="<?php print t('Close'); ?>">
      <% } %>
    </div>
  </div>

  <div class="ccd-chat-messages">
    <% messages.each(function(message) { %>
      <div class="ccd-chat-message" data-mid="<%= message.get('mid') %>">
        <div class="ccd-chat-time">[<%= message.formatDateTime('timestamp') %>]</div>
        <div class="ccd-chat-text"><span class="ccd-chat-name"><%- message.get('sender_name') %>:</span> <%- message.get('text') %></div>
      </div>
    <% }) %>

    <% if (session.get('closed') == 0 && session.get('agent_uid') == agentUid) { %>
      <div class="ccd-chat-input">
        <input type="text" class="ccd-text-input">
        <input type="button" class="ccd-send-button form-submit" value="<?php print t('Send'); ?>">
      </div>
    <% } %>
  </div>

</script>

<!-- Template for the loading indicator -->
<script type="text/template" id="ccd-loading-template">
  <span class="dot-1"></span>
  <span class="dot-2"></span>
  <span class="dot-3"></span>
</script>
