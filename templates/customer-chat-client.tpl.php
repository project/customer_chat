<div id="customer-chat-client">
  <div class="cc-chat">
  </div>
</div>

<!-- Template for the chat window -->
<script type="text/template" id="cc-chat-template">

  <% if (status == 'connecting') { %>
    <div class="cc-chat-status cc-status-inactive">
      <?php print t('Connecting...'); ?>
    </div>
  <% } else if (status == 'loadError') { %>
    <div class="cc-chat-status cc-status-inactive">
      <?php print t('An error occurred while initiating chat session. Please try again.'); ?>
    </div>
  <% } else if (status == 'noAgent') { %>
    <div class="cc-chat-status cc-status-inactive">
      <?php print t('There are no agents online at the moment. Please try again later.'); ?>
    </div>
  <% } else if (chatSession.get('closed') != 0) { %>
    <div class="cc-chat-status cc-status-inactive">
      <?php print t('This chat session has been closed.'); ?>
    </div>
  <% } else if (chatSession.get('agent_online')) { %>
    <div class="cc-chat-status cc-status-active">
      <?php print t('You have been connected. Your agent:'); ?> <%- chatSession.get('agent_name') %>
    </div>
  <% } else if (chatSession.get('agent_uid')) { %>
    <div class="cc-chat-status cc-status-inactive">
      <?php print t('You have been assigned to an agent who is currently offline. Please wait for your agent to connect.'); ?>
    </div>
  <% } else { %>
    <div class="cc-chat-status cc-status-inactive">
      <?php print t('Please wait. An agent will be with you shortly.'); ?>
    </div>
  <% } %>

  <div class="cc-chat-buttons">
    <% if (chatSession.get('closed') == 0) { %>
    <input type="button" class="cc-end-button form-submit" value="<?php print t('End chat'); ?>">
    <% } %>
  </div>

  <div class="cc-chat-messages">
    <% messages.each(function(message) { %>
    <div class="cc-chat-message">
      <div class="cc-chat-time">[<%= message.formatDateTime('timestamp') %>]</div>
      <div class="cc-chat-text"><span class="cc-chat-name"><%- message.get('sender_name') %>:</span> <%- message.get('text') %></div>
    </div>
    <% }) %>

    <% if (chatSession.get('closed') == 0 && chatSession.get('agent_online')) { %>
      <div class="cc-chat-input">
        <input type="text" class="cc-text-input">
        <input type="button" class="cc-send-button form-submit" value="<?php print t('Send'); ?>">
      </div>
    <% } %>
  </div>

</script>
