<?php

/**
 * Page callback for the agent panel page.
 */
function customer_chat_agent_dashboard_page() {
  global $user;

  drupal_add_library('customer_chat', 'customer_chat_agent');

  // @TODO: TRUE is for notify on disconnect. Check if needed.
  nodejs_send_content_channel_token('customer_chat_agent', TRUE);
  
  // Generate token for Services module.
  // See also _user_resource_get_token().
  $csrf_token = drupal_get_token('services');

  // Pass variables to javascript.
  $js_settings = array(
    'uid' => $user->uid,
    'agent_name' => $user->name,
    'csrf_token' => $csrf_token,
  );
  drupal_add_js(array('customer_chat' => $js_settings), 'setting');

  return array(
    '#theme' => 'customer_chat_agent_dashboard',
  );
}
