<?php

/**
 * Flag indicating that the user is a customer.
 */
define('CUSTOMER_CHAT_ROLE_CUSTOMER', 0);

/**
 * Flag indicating that the user is an agent.
 */
define('CUSTOMER_CHAT_ROLE_AGENT', 1);

/**
 * Implements hook_menu().
 */
function customer_chat_menu() {
  $items = array();

  $items['customer-chat/start'] = array(
    'title' => 'Start customer chat',
    'description' => 'Start customer chat.',
    'page callback' => 'customer_chat_start_page',
    'access arguments' => array('use customer chat'),
    'file' => 'customer_chat.pages.inc',
  );

  $items['customer-chat/chat/%customer_chat_session'] = array(
    'title' => 'Customer chat',
    'description' => 'Customer chat.',
    'page callback' => 'customer_chat_chat_page',
    'page arguments' => array(2),
    'access callback' => 'customer_chat_chat_page_access',
    'access arguments' => array(2),
    'type' => MENU_CALLBACK,
    'file' => 'customer_chat.pages.inc',
  );

  $items['admin/customer-chat'] = array(
    'title' => 'Agent dashboard',
    'description' => 'Customer chat agent dashboard.',
    'page callback' => 'customer_chat_agent_dashboard_page',
    'access arguments' => array('use customer chat agent dashboard'),
    'weight' => 100,
    'file' => 'customer_chat.agent.inc',
  );

  $items['admin/config/system/customer-chat'] = array(
    'title' => 'Customer chat',
    'description' => 'Configure customer chat.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('customer_chat_config_form'),
    'access arguments' => array('configure customer chat'),
    'file' => 'customer_chat.admin.inc',
  );

  $items['admin/config/system/customer-chat/settings'] = array(
    'title' => 'Settings',
    'description' => 'Customer chat settings.',
    'access arguments' => array('configure customer chat'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function customer_chat_permission() {
  return array(
    'use customer chat' => array(
      'title' => t('Use customer chat as a customer'),
    ),
    'use customer chat agent dashboard' => array(
      'title' => t('Use customer chat agent dashboard'),
      'description' => t('Access the customer chat dashboard and accept chat requests from customers.'),
    ),
    'configure customer chat' => array(
      'title' => t('Configure customer chat'),
      'description' => t('Access and modify customer chat configurations.'),
    ),
  );
}

/**
 * Access callback for the chat page.
 */
function customer_chat_chat_page_access($chat_session) {
  if (!user_access('use customer chat')) {
    return FALSE;
  }
  else if (!customer_chat_is_agent() && !customer_chat_is_own_session($chat_session)) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * Implements hook_block_info().
 */
function customer_chat_block_info() {
  return array(
    'customer_chat_start' => array(
      'info' => t('Customer Chat: Start chat block'),
    )
  );
}

/**
 * Implements hook_block_view().
 */
function customer_chat_block_view($delta) {
  $block = array();

  switch ($delta) {
    case 'customer_chat_start':
      if (user_access('use customer chat')) {
        $block['subject'] = t('Customer chat');
        $block['content'] = array(
          '#theme' => 'customer_chat_start_chat_block'
        );
      }
      break;
  }

  return $block;
}

/**
 * Implements hook_entity_info().
 */
function customer_chat_entity_info() {
  return array(
    'customer_chat_session' => array(
      'label' => t('Customer chat session'),
      'base table' => 'customer_chat_session',
      'entity keys' => array(
        'id' => 'sid',
      ),
      'fieldable' => TRUE,
      'bundles' => array(
        'customer_chat_session' => array(
          'label' => t('Customer chat session'),
          'admin' => array(
            'path' => 'admin/config/system/customer-chat',
            'access arguments' => array('configure customer chat'),
          ),
        ),
      ),
      'load hook' => 'customer_chat_session_load',
    ),
  );
}

/**
 * Loads a chat session object.
 *
 * @param $sid
 *   A chat session object id.
 * @param $reset
 *   If TRUE, the internal cache will be reset.
 *
 * @return
 *   The loaded object; or FALSE on error.
 */
function customer_chat_session_load($sid, $reset = FALSE) {
  $objects = entity_load('customer_chat_session', array($sid), array(), $reset);
  return $objects ? reset($objects) : FALSE;
}

/**
 * Loads multiple chat session objects.
 *
 * @param $sids
 *   An array of sids.
 *
 * @return
 *   Array of loaded entities.
 */
function customer_chat_session_load_multiple($sids) {
  return entity_load('customer_chat_session', $sids);
}

/**
 * Implements hook_customer_chat_session_load().
 */
function customer_chat_customer_chat_session_load($entities) {
  // Ensure the messages attribute always exists; collect uids to load.
  $agent_uids = array();
  foreach ($entities as $sid => $entity) {
    $entity->messages = array();

    if ($entity->agent_uid) {
      $agent_uids[] = $entity->agent_uid;
    }
  }

  // Load names for users who are assigned agents.
  if (!empty($agent_uids)) {
    $agent_names = db_select('users', 'u')
      ->fields('u', array('uid', 'name'))
      ->condition('uid', $agent_uids, 'IN')
      ->execute()
      ->fetchAllAssoc('uid');
  }

  // Find messages belonging to the loaded sessions.
  $query = db_select('customer_chat_message', 'm')
    ->fields('m')
    ->condition('m.sid', array_keys($entities), 'IN');

  // Join users table in order to get the sender's name.
  $query->leftJoin('users', 'u', 'u.uid = m.sender_uid');
  $query->addField('u', 'name', 'sender_name');
  $query->orderBy('m.mid', 'ASC');

  $messages = $query->execute()
    ->fetchAllAssoc('mid');

  // Assign messages to entities.
  foreach ($messages as $message) {
    if (empty($message->sender_name)) {
      // If the sender is not a registered user, copy the name specified in the
      // chat session.
      $message->sender_name = $entities[$message->sid]->customer_name;
    }

    $entities[$message->sid]->messages[] = $message;
  }

  // Get fields added to the entity type.
  $field_instances = field_info_instances('customer_chat_session', 'customer_chat_session');

  // Add derived data.
  foreach ($entities as $sid => $entity) {
    // Agent name.
    if (!empty($entity->agent_uid)) {
      $entity->agent_name = isset($agent_names[$entity->agent_uid]) ? $agent_names[$entity->agent_uid]->name : t('Unknown agent');
    }
    else {
      $entity->agent_name = t('No agent assigned');
    }

    // Customer's user profile link.
    if ($entity->customer_uid) {
      $entity->customer_profile_link = url('user/' . $entity->customer_uid);
    }

    // New indicator. (TRUE when the chat session is unassigned, or the last
    // message is from the customer)
    if (!$entity->agent_uid) {
      $entity->has_new = TRUE;
    }
    else if (!empty($entity->messages) && $entity->messages[count($entity->messages) - 1]->sender_role == CUSTOMER_CHAT_ROLE_CUSTOMER) {
      $entity->has_new = TRUE;
    }
    else {
      $entity->has_new = FALSE;
    }

    // Render custom fields into one field that can be displayed by JS.
    $rendered_fields = '';
    foreach ($field_instances as $field) {
      $field_view = field_view_field('customer_chat_session', $entity, $field['field_name'], 'default');
      $rendered_fields .= drupal_render($field_view);
    }
    // Note: Because this field is rendered here, it won't need to be escaped
    // in JS.
    $entity->custom_data = $rendered_fields;
  }

}

/**
 * Saves a chat session object.
 * (Does not save the messages array)
 *
 * @param $entity
 *   A chat session entity object.
 */
function customer_chat_session_save($entity) {
  // Perform presave actions on fields.
  field_attach_presave('customer_chat_session', $entity);

  // Allow modules to act on the entity before saving.
  module_invoke_all('entity_presave', $entity, 'customer_chat_session');

  // Store base properties.
  if (empty($entity->sid)) {
    // Creating a new entity.
    $entity->created = REQUEST_TIME;

    drupal_write_record('customer_chat_session', $entity);
    $op = 'insert';
  }
  else {
    // Updating an existing entity.
    drupal_write_record('customer_chat_session', $entity, 'sid');
    $op = 'update';
  }

  // Allow modules to act after inserting/updating.
  module_invoke_all('entity_' . $op, $entity, 'customer_chat_session');

  // Save fields. (Call field_attach_update or field_attach_insert.)
  $function = 'field_attach_' . $op;
  $function('customer_chat_session', $entity);
}

/**
 * Implements hook_library().
 */
function customer_chat_library() {
  // @TODO: Add check for backbonejs/underscore presence and jquery update
  // selected version in hook_requirements.
  $libraries = array();

  // Agent library.
  // Note: Js files need to load after Node.js' files, so group and weight are
  // set.
  $libraries['customer_chat_agent'] = array(
    'title' => 'Customer chat agent',
    'version' => '1',
    'js' => array(
      libraries_get_path('underscore') . '/underscore.js' => array(),
      libraries_get_path('backbone') . '/backbone.js' => array(),
      drupal_get_path('module', 'customer_chat') . '/js/customer_chat.common.js' => array('group' => JS_DEFAULT, 'weight' => 100),
      drupal_get_path('module', 'customer_chat') . '/js/customer_chat.agent.js' => array('group' => JS_DEFAULT, 'weight' => 100),
      drupal_get_path('module', 'customer_chat') . '/js/customer_chat.js' => array('group' => JS_DEFAULT, 'weight' => 100),
    ),
    'css' => array(
      drupal_get_path('module', 'customer_chat') . '/css/customer_chat.agent.css' => array(),
    ),
  );

  $libraries['customer_chat_client'] = array(
    'title' => 'Customer chat client',
    'version' => '1',
    'js' => array(
      libraries_get_path('underscore') . '/underscore.js' => array(),
      libraries_get_path('backbone') . '/backbone.js' => array(),
      drupal_get_path('module', 'customer_chat') . '/js/customer_chat.common.js' => array('group' => JS_DEFAULT, 'weight' => 100),
      drupal_get_path('module', 'customer_chat') . '/js/customer_chat.client.js' => array('group' => JS_DEFAULT, 'weight' => 100),
      drupal_get_path('module', 'customer_chat') . '/js/customer_chat.js' => array('group' => JS_DEFAULT, 'weight' => 100),
    ),
    'css' => array(
      drupal_get_path('module', 'customer_chat') . '/css/customer_chat.client.css' => array(),
    ),
    'dependencies' => array(
      array('system', 'jquery.cookie'),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_theme().
 */
function customer_chat_theme() {
  $template_path = drupal_get_path('module', 'customer_chat') . '/templates';

  return array(
    'customer_chat_agent_dashboard' => array(
      'variables' => array('content' => NULL),
      'template' => 'customer-chat-agent-dashboard',
      'path' => $template_path,
    ),
    'customer_chat_client' => array(
      'variables' => array('content' => NULL),
      'template' => 'customer-chat-client',
      'path' => $template_path,
    ),
    'customer_chat_window' => array(
      'render element' => 'page',
      'template' => 'customer-chat-window',
      'path' => $template_path,
    ),
    'customer_chat_start_chat_block' => array(
      'variables' => array(),
      'template' => 'customer-chat-start-chat-block',
      'path' => $template_path,
    ),
  );
}

/**
 * Returns an array of user ids that can act as agents. (I.e. users that have
 * the 'use customer chat agent dashboard' permission.)
 */
function customer_chat_get_agents() {
  // Find roles with agent permission.
  $roles = user_roles(TRUE, 'use customer chat agent dashboard');
  if (empty($roles)) {
    return array();
  }

  // Find users with the agent role.
  $uids = db_select('users_roles', 'ur')
    ->fields('ur', array('uid'))
    ->condition('ur.rid', array_keys($roles), 'IN')
    ->execute()
    ->fetchCol();

  return $uids;
}

/**
 * Checks if a user is an agent.
 *
 * @param $account
 *   The user account to check. Defaults to the logged in user.
 *
 * @return
 *   TRUE or FALSE.
 */
function customer_chat_is_agent($account = NULL) {
  global $user;

  if (!isset($account)) {
    $account = $user;
  }
  return user_access('use customer chat agent dashboard', $account);
}

/**
 * Checks if an agent is online.
 *
 * @param $uid
 *   Agent's user id.
 *
 * @return
 *   TRUE, if the agent is online; FALSE otherwise.
 */
function customer_chat_agent_is_online($uid) {
  $online_agents = customer_chat_get_online_agents();
  return in_array($uid, $online_agents) ? 1 : 0;
}

/**
 * Returns a list of online agents.
 *
 * @return
 *   Array of uids.
 */
function customer_chat_get_online_agents() {
  static $online_uids = NULL;

  if (!isset($online_uids)) {
    $connected_users = nodejs_get_content_channel_users('customer_chat_agent');

    if (!isset($connected_users['uids'])) {
      $connected_users['uids'] = array();
    }

    $online_uids = $connected_users['uids'];
  }

  return $online_uids;
}

/**
 * Returns a list of online clients for a chat session.
 *
 * @return
 *   Array of uids.
 */
function customer_chat_get_online_clients($sid) {
  static $online_uids = array();

  if (!isset($online_uids[$sid])) {
    $connected_users = nodejs_get_content_channel_users('customer_chat_' . $sid);

    if (!isset($connected_users['uids'])) {
      $connected_users['uids'] = array();
    }

    $online_uids[$sid] = $connected_users['uids'];

    if (!empty($connected_users['authTokens'])) {
      // For anonymous users, we receive the auth tokens (generated by nodejs).
      // These users cannot by identified individually; just and an entry for
      // anonymous.
      $online_uids[$sid][] = 0;
    }
  }

  return $online_uids[$sid];
}

/**
 * Implements hook_ctools_plugin_api().
 */
function customer_chat_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_services_resources().
 */
function customer_chat_services_resources() {
  $resources = array(
    '#api_version' => 3002,
  );

  $resources['customer_chat_agent'] = array(
    'operations' => array(
      'index' => array(
        'help' => 'List agents.',
        'file' => array(
          'name' => 'customer_chat.resources',
          'type' => 'inc',
          'module' => 'customer_chat'
        ),
        'callback' => 'customer_chat_resource_agent_index',
        'args' => array(
          array(
            'name' => 'offline',
            'optional' => TRUE,
            'type' => 'int',
            'description' => 'Indicates whether offline agents should be included in the list.',
            'default value' => 0,
            'source' => array('param' => 'offline'),
          ),
        ),
        'access callback' => 'customer_chat_resource_agent_access',
        'access arguments' => array('index'),
      ),
    ),
  );

  $resources['customer_chat_session'] = array(
    'operations' => array(
      'retrieve' => array(
        'help' => 'Retrieve a chat session',
        'file' => array(
          'name' => 'customer_chat.resources',
          'type' => 'inc',
          'module' => 'customer_chat'
        ),
        'callback' => 'customer_chat_resource_chat_session_retrieve',
        'args' => array(
          array(
            'name' => 'sid',
            'optional' => FALSE,
            'source' => array('path' => 0),
            'type' => 'int',
            'description' => 'The id of the chat session.',
          ),
        ),
        'access callback' => 'customer_chat_resource_chat_session_access',
        'access arguments' => array('view'),
        'access arguments append' => TRUE,
      ),
      'index' => array(
        'help' => 'List all chat sessions.',
        'file' => array(
          'name' => 'customer_chat.resources',
          'type' => 'inc',
          'module' => 'customer_chat'
        ),
        'callback' => 'customer_chat_resource_chat_session_index',
        'args' => array(
          array(
            'name' => 'type',
            'optional' => TRUE,
            'type' => 'string',
            'description' => 'The type of sessions to include (active, all_active, closed).',
            'default value' => 'active',
            'source' => array('param' => 'type'),
          ),
          array(
            'name' => 'page',
            'optional' => TRUE,
            'type' => 'int',
            'description' => 'The page to retrieve.',
            'default value' => 0,
            'source' => array('param' => 'page'),
          ),
          array(
            'name' => 'limit',
            'optional' => TRUE,
            'type' => 'int',
            'description' => 'The number of items to retrieve per page.',
            'default value' => 0,
            'source' => array('param' => 'limit'),
          ),
        ),
        'access callback' => 'customer_chat_resource_chat_session_access',
        'access arguments' => array('index'),
      ),
    ),
    'actions' => array(
      'register_agent' => array(
        'help' => 'Prepares an agent to receive chats. Should be called after an agent connects.',
        'file' => array(
          'name' => 'customer_chat.resources',
          'type' => 'inc',
          'module' => 'customer_chat'
        ),
        'callback' => 'customer_chat_resource_chat_session_register_agent',
        'args' => array(
        ),
        'access callback' => 'customer_chat_resource_chat_session_access',
        'access arguments' => array('register_agent'),
      ),
    ),
    'targeted_actions' => array(
      'post_message' => array(
        'help' => 'Post a message to a chat session.',
        'file' => array(
          'name' => 'customer_chat.resources',
          'type' => 'inc',
          'module' => 'customer_chat'
        ),
        'callback' => 'customer_chat_resource_chat_session_post_message',
        'access callback' => 'customer_chat_resource_chat_session_access',
        'access arguments' => array('post_message'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'sid',
            'optional' => FALSE,
            'source' => array('path' => 0),
            'type' => 'int',
            'description' => 'The id of the chat session.',
          ),
          array(
            'name' => 'text',
            'optional' => FALSE,
            'source' => array('data' => 'text'),
            'description' => 'The content of the message.',
            'type' => 'string',
          ),
        ),
      ),
      'register_agent' => array(
        'help' => 'Registers an agent for updates in a chat session.',
        'file' => array(
          'name' => 'customer_chat.resources',
          'type' => 'inc',
          'module' => 'customer_chat'
        ),
        'callback' => 'customer_chat_resource_chat_session_register_agent',
        'args' => array(
          array(
            'name' => 'sid',
            'optional' => FALSE,
            'source' => array('path' => 0),
            'type' => 'int',
            'description' => 'The id of the chat session.',
          ),
        ),
        'access callback' => 'customer_chat_resource_chat_session_access',
        'access arguments' => array('register_agent_targeted'),
        'access arguments append' => TRUE,
      ),
      'assign_agent' => array(
        'help' => 'Assigns the chat session to an agent.',
        'file' => array(
          'name' => 'customer_chat.resources',
          'type' => 'inc',
          'module' => 'customer_chat'
        ),
        'callback' => 'customer_chat_resource_chat_session_assign_agent',
        'access callback' => 'customer_chat_resource_chat_session_access',
        'access arguments' => array('assign_agent'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'sid',
            'optional' => FALSE,
            'source' => array('path' => 0),
            'type' => 'int',
            'description' => 'The id of the chat session.',
          ),
          array(
            'name' => 'agent_uid',
            'optional' => FALSE,
            'source' => array('data' => 'agent_uid'),
            'type' => 'int',
            'description' => 'The id of the agent to assign to the chat session.',
          ),
        ),
      ),
      'start_chat' => array(
        'help' => 'Notifies participants that the client is ready for chatting.',
        'file' => array(
          'name' => 'customer_chat.resources',
          'type' => 'inc',
          'module' => 'customer_chat'
        ),
        'callback' => 'customer_chat_resource_chat_session_start_chat',
        'access callback' => 'customer_chat_resource_chat_session_access',
        'access arguments' => array('start_chat'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'sid',
            'optional' => FALSE,
            'source' => array('path' => 0),
            'type' => 'int',
            'description' => 'The id of the chat session.',
          ),
        ),
      ),
      'close_chat' => array(
        'help' => 'Closes the chat session.',
        'file' => array(
          'name' => 'customer_chat.resources',
          'type' => 'inc',
          'module' => 'customer_chat'
        ),
        'callback' => 'customer_chat_resource_chat_session_close',
        'access callback' => 'customer_chat_resource_chat_session_access',
        'access arguments' => array('close_chat'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'sid',
            'optional' => FALSE,
            'source' => array('path' => 0),
            'type' => 'int',
            'description' => 'The id of the chat session.',
          ),
        ),
      ),
    ),
  );

  return $resources;
}

/**
 * Implements hook_page_alter().
 */
function customer_chat_page_alter(&$page) {
  $item = menu_get_item();
  if ($item['path'] == 'customer-chat/chat/%' && variable_get('customer_chat_window_style', 'minimal') == 'minimal') {
    // Override the page theme with the chat window theme.
    unset($page['#type']);
    $page['#theme'] = 'customer_chat_window';

    // Ensure the main content is available. (Otherwise it may or may not have
    // been put in the default content region.)
    // Note that regions are not rendered in this template by default.
    $page['chat_window_content'] = drupal_set_page_content();
  }
}

/**
 * Implements hook_preprocess_hook().
 */
function customer_chat_preprocess_customer_chat_start_chat_block(&$vars) {
  $online_agents = customer_chat_get_online_agents();

  if (empty($online_agents)) {
    $vars['online'] = FALSE;
    $vars['offline_message'] = t('There are no agents online at this moment. Please check back later.');
  }
  else {
    $vars['online'] = TRUE;
    $vars['start_link'] = l(t('Start chat'), 'customer-chat/start');
  }
}

/**
 * Implements hook_preprocess_hook().
 */
function customer_chat_preprocess_customer_chat_window(&$vars) {
  // Add helper variables for use in the template.
  // (These are the same variables as in the page template.)
  $vars['title'] = drupal_get_title();
  $vars['front_page'] = url();
  $vars['base_path'] = base_path();
  $vars['logo'] = theme_get_setting('logo');
  $vars['site_name'] = (theme_get_setting('toggle_name') ? filter_xss_admin(variable_get('site_name', 'Drupal')) : '');
}

/**
 * Implements hook_process_hook().
 */
function customer_chat_process_customer_chat_window(&$vars) {
  // The chat window theme replaces the generic page theme, so we need to take
  // care of message display.
  if (!isset($vars['messages'])) {
    $vars['messages'] = $vars['page']['#show_messages'] ? theme('status_messages') : '';
  }
}

/**
 * Detects the user's OS from a user agent string.
 *
 * @param $user_agent
 *  User agent string sent by the user's browser.
 *
 * @return
 *  Detected OS name.
 */
function customer_chat_detect_os($user_agent) {
  $patterns = array(
    'iOS' => 'iPhone|iPod|iPad',
    'Android' => '[Aa]ndroid',
    'WindowsMobile' => 'Windows CE.*|Window Mobile|Windows Phone [0-9.]+|WCE;',
    'WindowsPhone' => 'Windows Phone 8.1|Windows Phone 8.0|Windows Phone OS|XBLWP7|ZuneWP7|Windows NT 6.[23]; ARM;',
    'BlackBerry' => 'BlackBerry|BB10|rim[0-9]+',
    'Symbian' => 'Symbian|SymbOS|Series60|Series40|SYB-[0-9]+|S60',
    'Windows 95' => '(Windows 95)|(Win95)|(Windows_95)',
    'Windows 98' => '(Windows 98)|(Win98)',
    'Windows 2000' => '(Windows NT 5.0)|(Windows 2000)',
    'Windows XP' => '(Windows NT 5.1)|(Windows XP)',
    'Windows Server 2003' => '(Windows NT 5.2)',
    'Windows Vista' => '(Windows NT 6.0)',
    'Windows 7' => '(Windows NT 6.1)',
    'Windows 8' => '(Windows NT 6.2)',
    'Windows 8.1' => '(Windows NT 6.3)',
    'Windows 10' => '(Windows NT 10.0)',
    'Windows NT 4.0' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
    'Windows ME' => 'Windows ME',
    'Open BSD' => 'OpenBSD',
    'Sun OS' => 'SunOS',
    'Linux' => '(Linux)|(X11)',
    'Mac OS' => '(Mac_PowerPC)|(Macintosh)',
    'QNX' => 'QNX',
    'BeOS' => 'BeOS',
    'OS/2' => 'OS/2',
    'Mobile' => '[Mm]obile',
    'Web crawler'=> '(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves/Teoma)|(ia_archiver)',
  );

  foreach ($patterns as $name => $pattern) {
    if (preg_match('@' . $pattern . '@', $user_agent)) {
      return $name;
    }
  }

  return t('Unknown');
}

/**
 * Detects the user's browser from a user agent string.
 *
 * @param $user_agent
 *  User agent string sent by the user's browser.
 *
 * @return
 *  Detected browser name.
 */
function customer_chat_detect_browser($user_agent) {
  $patterns = array(
    'Chrome Mobile' => 'CrMo|CriOS|Android.*Chrome\/',
    'Opera Mobile' => 'Opera.*Mini|Opera.*Mobi|Android.*Opera|Mobile.*OPR\/[0-9.]+|Coast\/[0-9.]+',
    'IE Mobile' => 'IEMobile|MSIEMobile',
    'Firefox Mobile' => 'fennec|firefox.*maemo|(Mobile|Tablet).*Firefox|Firefox.*Mobile',
    'Safari Mobile' => 'Version.*Mobile.*Safari|Safari.*Mobile|MobileSafari',
    'Chrome' => 'Chrome\/',
    'Firefox' => 'Firefox\/',
    'Opera' => 'Opera\/',
    'Safari' => 'Safari\/',
    'Internet Explorer' => 'MSIE|Trident\/',
  );

  foreach ($patterns as $name => $pattern) {
    if (preg_match('@' . $pattern . '@', $user_agent)) {
      return $name;
    }
  }

  return t('Unknown');
}

/**
 * Determines whether a chat session belongs to the current user.
 *
 * @param $chat_session
 *   A chat session object.
 *
 * @return
 *   TRUE or FALSE.
 */
function customer_chat_is_own_session($chat_session) {
  global $user;

  // For logged in users, check the user id.
  if ($user->uid && $user->uid != $chat_session->customer_uid) {
    return FALSE;
  }

  // For non-authenticated user, check the visitor id.
  if (!$user->uid && customer_chat_get_visitor_id() != $chat_session->visitor_id) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Returns a visitor id that identifies the current user, even if they are not
 * logged in.
 */
function customer_chat_get_visitor_id() {
  return session_id();
}
