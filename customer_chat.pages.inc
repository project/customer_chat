<?php

/**
 * Page callback for the start chat form.
 */
function customer_chat_start_page() {
  // This form requests initial info form the visitor.
  // If info is already entered, redirect to the chat page.

  return drupal_get_form('customer_chat_start_form');
}

/**
 * Form callback for the start chat form.
 */
function customer_chat_start_form($form, $form_state) {
  global $user;

  $form = array();

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => ($user->uid ? $user->name : ''),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -110,
    '#disabled' => ($user->uid != 0),
  );

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#default_value' => ($user->uid ? $user->mail : ''),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -100,
    '#disabled' => ($user->uid != 0),
  );

  $form['start'] = array(
    '#type' => 'submit',
    '#value' => 'Start chat',
    '#weight' => 100,
  );

  field_attach_form('customer_chat_session', NULL, $form, $form_state);

  return $form;
}

/**
 * Submit callback for the start chat form.
 */
function customer_chat_start_form_submit($form, &$form_state) {
  global $user;

  // Need to start session because anonymous chats are authenticated using the
  // php session.
  drupal_session_start();

  if (!$user->uid) {
    // visitor_id is used to identify anonymous users. Visitors using the
    // same session are considered the same visitors.
    $visitor_id = customer_chat_get_visitor_id();
  }
  else {
    $visitor_id = '';
  }

  $entity = (object) array(
    'customer_uid' => $user->uid,
    'visitor_id' => $visitor_id,
    'customer_name' => $user->uid ? $user->name : $form_state['values']['name'],
    'customer_email' => $user->uid ? $user->mail : $form_state['values']['email'],
    //'agent_uid' => '',
    'ip' => $_SERVER['REMOTE_ADDR'],
    'os' => customer_chat_detect_os($_SERVER['HTTP_USER_AGENT']),
    'browser' => customer_chat_detect_browser($_SERVER['HTTP_USER_AGENT']),
    'browser_user_agent' => $_SERVER['HTTP_USER_AGENT'],
    'online' => 0,
    'last_viewed' => '',
    'closed' => 0,
  );

  // Prepare fields with data in the submitted form.
  entity_form_submit_build_entity('customer_chat_session', $entity, $form, $form_state);

  customer_chat_session_save($entity);

  if (empty($entity->sid)) {
    drupal_set_message(t('An error occurred while initiating your chat session.'), 'error');
    return;
  }

  drupal_goto('customer-chat/chat/' . $entity->sid);
}

/**
 * Page callback for the customer chat window content.
 */
function customer_chat_chat_page($chat_session) {
  drupal_add_library('customer_chat', 'customer_chat_client');

  // Generate token for Services module.
  // See also _user_resource_get_token().
  $csrf_token = drupal_get_token('services');

  nodejs_send_content_channel_token('customer_chat_customer', TRUE);
  nodejs_send_content_channel_token('customer_chat_' . $chat_session->sid, TRUE);

  // Pass variables to javascript.
  $js_settings = array(
    'sid' => $chat_session->sid,
    'csrf_token' => $csrf_token,
  );
  drupal_add_js(array('customer_chat' => $js_settings), 'setting');
  
  return array(
    '#theme' => 'customer_chat_client',
  );
}
