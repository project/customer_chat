<?php

/**
 * Form callback for the configuration page.
 */
function customer_chat_config_form($form, $form_state) {
  $form = array();

  //$form['customer_chat_agent_role'] = array(
  //  '#type' => 'select',
  //  '#title' => t('Agent role'),
  //  '#default_value' => variable_get('customer_chat_agent_role', 0),
  //  '#description' => t('Select agent role.'),
  //  '#required' => TRUE,
  //);

  $form['customer_chat_window_style'] = array(
    '#type' => 'radios',
    '#title' => t('Customer chat window style'),
    '#options' => array(
      'minimal' => t('Minimal'),
      'page' => t('Regular page'),
    ),
    '#default_value' => variable_get('customer_chat_window_style', 'minimal'),
    '#description' => t('Choose how the chat window will look like. If "Regular page" is selected, the chat window will have the same elements as all other pages. Otherwise, it will only have a minimal header and no other content.'),
  );

  return system_settings_form($form);
}
